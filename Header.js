import React, { Component } from "react";
import { connect } from "react-redux";
import { userLogout } from "../redux/auth/actions";

class Header extends Component {
  onLogoutClick = (event) => {
    event.preventDefault();
    this.props.userLogout();
    console.log("history", this.props);
  };

  render() {
    console.log("authenticatedUser--->", this.props.authenticatedUser);
    return (
      <div className="Header">
        <header
          id="m_header"
          className="m-grid__item m-header "
          m-minimize-offset="200"
          m-minimize-mobile-offset="200"
        >
          <div className="m-container m-container--fluid m-container--full-height">
            <div className="m-stack m-stack--ver m-stack--desktop">
              <div className="m-stack__item m-brand  m-brand--skin-dark ">
                <div className="m-stack m-stack--ver m-stack--general">
                  <div className="m-stack__item m-stack__item--middle m-brand__logo">
                    <a
                      href="/#"
                      className="m-brand__logo-wrapper"
                      onClick={() => this.props.history.push("/")}
                    >
                      <img
                        alt=""
                        src="/images/logo_dash.svg"
                        style={{ width: "107px" }}
                      />
                    </a>
                  </div>
                  <div className="m-stack__item m-stack__item--middle m-brand__tools">
                    <a
                      href="/#"
                      id="m_aside_left_minimize_toggle"
                      className="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  "
                    >
                      <span />
                    </a>
                    <a
                      href="/#"
                      id="m_aside_left_offcanvas_toggle"
                      className="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block"
                    >
                      <span />
                    </a>
                    <a
                      href="/#"
                      id="m_aside_header_topbar_mobile_toggle"
                      className="m-brand__icon m--visible-tablet-and-mobile-inline-block"
                    >
                      <i className="flaticon-more" />
                    </a>
                  </div>
                </div>
              </div>
              <div
                className="m-stack__item m-stack__item--fluid m-header-head"
                id="m_header_nav"
                style={{ backgroundColor: "#fff" }}
              >
                <button
                  className="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark "
                  id="m_aside_header_menu_mobile_close_btn"
                >
                  <i className="la la-close" />
                </button>
                <div
                  id="m_header_topbar"
                  className="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid"
                >
                  <div className="m-stack__item m-topbar__nav-wrapper">
                    <ul className="m-topbar__nav m-nav m-nav--inline">
                      <li
                        className="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                        m-dropdown-toggle="click"
                      >
                        <a href="/#" className="m-nav__link m-dropdown__toggle">
                          <span className="m-topbar__userpic">
                            <img
                              src="/images/avatar.png"
                              className="m--img-rounded m--marginless"
                              alt=""
                            />
                          </span>
                        </a>
                        <div className="m-dropdown__wrapper">
                          <span className="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" />
                          <div className="m-dropdown__inner">
                            <div
                              className="m-dropdown__header m--align-center"
                              style={{
                                background:
                                  "url(/assets/app/media/img/misc/user_profile_bg.jpg)",
                                backgroundSize: "cover",
                              }}
                            >
                              <div className="m-card-user m-card-user--skin-dark">
                                <div className="m-card-user__pic">
                                  <img
                                    src="/images/avatar.png"
                                    className="m--img-rounded m--marginless"
                                    alt=""
                                  />
                                </div>
                                <div className="m-card-user__details">
                                  <span className="m-card-user__name m--font-weight-500">
                                    {this.props.authenticatedUser.name
                                      ? this.props.authenticatedUser.name
                                          .first +
                                        " " +
                                        this.props.authenticatedUser.name.last
                                      : ""}
                                  </span>
                                  <a
                                    href="/#"
                                    className="m-card-user__email m--font-weight-300 m-link"
                                  >
                                    {this.props.authenticatedUser.email}
                                  </a>
                                </div>
                              </div>
                            </div>
                            <div className="m-dropdown__body">
                              <div className="m-dropdown__content">
                                <ul className="m-nav m-nav--skin-light">
                                  <li className="m-nav__section m--hide">
                                    <span className="m-nav__section-text">
                                      Section
                                    </span>
                                  </li>
                                  <li className="m-nav__item">
                                    <a
                                      href={`/dashboard/users/${this.props.authenticatedUser._id}`}
                                      className="m-nav__link"
                                    >
                                      <i className="m-nav__link-icon flaticon-profile-1" />
                                      <span className="m-nav__link-title">
                                        <span className="m-nav__link-wrap">
                                          <span className="m-nav__link-text">
                                            My Profile
                                          </span>
                                        </span>
                                      </span>
                                    </a>
                                  </li>
                                  <li className="m-nav__item">
                                    <a href="/#" className="m-nav__link">
                                      <i className="m-nav__link-icon flaticon-share" />
                                      <span className="m-nav__link-text">
                                        Activity
                                      </span>
                                    </a>
                                  </li>
                                  <li className="m-nav__item">
                                    <a href="/#" className="m-nav__link">
                                      <i className="m-nav__link-icon flaticon-chat-1" />
                                      <span className="m-nav__link-text">
                                        Messages
                                      </span>
                                    </a>
                                  </li>
                                  <li className="m-nav__separator m-nav__separator--fit" />
                                  <li className="m-nav__item">
                                    <a href="/#" className="m-nav__link">
                                      <i className="m-nav__link-icon flaticon-lifebuoy" />
                                      <span className="m-nav__link-text">
                                        Support
                                      </span>
                                    </a>
                                  </li>
                                  <li className="m-nav__separator m-nav__separator--fit" />
                                  <li className="m-nav__item">
                                    <a
                                      href="/#"
                                      className="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder"
                                      onClick={this.onLogoutClick}
                                    >
                                      Logout
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  authenticatedUser: state.auth.authenticatedUser,
});

export default connect(mapStateToProps, { userLogout })(Header);
